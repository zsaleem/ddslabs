## Requirements
Below is the stack I used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - JavaScript
 - Here Map API
 - ES6+
 - CSS
 - BEM
 - CSS Grid
 - CSS Animations
 - CSS Media Queries
 - Font Awesome
 - Mobile first responsive
 - Git
 - Gitflow
 - Gitlab
 - NPM/Yarn
 - Sublime Text
 - Font Awesome
 - Mac OS X
 - Google Chrome Incognito
 - Webpack
 - Babel
 - eslint
 - nodejs(16.4.0)

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. cd into `/ddslabs` directory.
    3. Add `Here map package to npm registery` by using command `npm config set @here:registry https://repo.platform.here.com/artifactory/api/npm/maps-api-for-javascript/`. Very important step.
    4. Run `npm install or yarn` command to download and install all dependencies.
    5. To run this project use `yarn serve or npm run serve` command in command line.
    6. To build the project for production run `yarn build:prod or npm run build:prod` command. This will build the app for production and put all the files in `/dist` folder.
    7. To run production ready version on local environment, run `yarn serve:prod or npm run serve:prod`. Then go to `http://localhost:8080`.

## Description
Above steps, in getting started section, will install all the dependencies required for this project to run.

For this project I setup the whole development environment from scratch using `webpack` and `babel`.

In this task I used `ES6+ syntax`. I developed the entire project inside `/ddslabs` folder. All the components for this project resides in `/src/components/` folder. Every component has its own folder with its `scripts` and `styles` folder which contains all the `css` and `javascript` associated with that component.

The entire html form for this project is inside `/src/index.html`.

I also created a video from this project which shows the app in action. [Click here](https://www.youtube.com/watch?v=--UXQTXQvwk) to watch it in action.

### Development Environments
As I mentioned I developed the whole development environment for this project. In order to achieve that I used `Webpack`. I wrote the `webpack.config.js` file from scratch. What this file basically does that it checks the current `mode` of the running server. If it is `development` then load the development specific web pack file which is `/build/development.js` file. If the `mode` is `production` then load production specific file which is `/build/production.js`.

For `development` environment, there is no need to build and minify all the resources simply go ahead and execute the project in local environment as is.

For `production` environment, I am adding a tarser plugin to minify all the resources for production release.

The `/build/common.js` file is basically contains the common code for both `development` and `production` environment with minor changes just to follow DRY practices.

### Architecture
For this task I followed the component based architecture. Since I it was instructed to specifically use `JavaScript, HTML and CSS` for this task so I had no options in choosing modern languages with modern frameworks. So I wrote all the components in `/src/components/` folder. For CSS part, I used `BEM` methodology/naming convention which allowed me to write reusable HTML and CSS.

### Performance
In terms of performance, as I mentioned I am using webpack to build the project for production. So it minifies all the resources which reduces its files sizes which loads faster on the network.

### Development process
The development process that I followed for this project is `Gitflow` work flow. That means, I have two git branches `master` and `develop`. So I created a `feature branch` from `develop` branch. I develop the whole feature in that branch. Once I am done with it. I `merge` that branch with `develop` branch and push it to remote repo with `develop` branch. When I need to release new feature, I create a new branch `release` with a version number on it. Then I merge that `release` branch into master branch and push it to remote repo's `master` branch.

### Code Quality
I wrote the entire project in components architecture which means everything is a component. I wrote all the code very readable and easy to follow. In addition, I used `eslint` to keep the quality of the code intact.

## Notes
Since it was mentioned in task email that I need to develop a `simplified version` of order form. For this I needed a definition of simplification which I did not have.

If the simplification meant that patients need to enter as less information as possible then for that I needed information from the vendor that what information are necessary to be asked from patient at the time of placing order and I'd remove the unnecessary input fields from the form.

However, if the simplification means that patients need to enter few details even though there are bunch of input fields in the form so I made that possible by auto filling the address fields when they input their address.

If simplification means that the form should have a better UX and UI and in that case I convert the entire form into better UX/UI i.e. Patients can view errors if they haven't enter a required field. The labels of inputs are animated away when patient clicks in the input field. Radio buttons have icons which better represents what each label means etc.

