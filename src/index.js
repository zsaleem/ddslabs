import { library, dom } from '@fortawesome/fontawesome-svg-core';
import {
	faBriefcaseMedical,
	faSyringe,
	faStethoscope,
} from '@fortawesome/free-solid-svg-icons';
import './index.css';
import Scripts from './scripts/scripts.js';
import Input from './components/input/';
import Address from './components/address/';
import Select from './components/select/';
import Radio from './components/radio/';
import Button from './components/button/';
import './form.css';

library.add(faBriefcaseMedical);
library.add(faSyringe);
library.add(faStethoscope);
dom.watch();

