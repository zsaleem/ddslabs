export default async function Input() {
	const blocks = document.querySelectorAll('.input');
	const events = 'keydown blur focus'.split(' ');

	for (const block of blocks) {
		const label = await filter(block.children, 'input__label');
		const input = await filter(label.children, 'input__field');
		const labelText = await filter(label.children, 'input__label-text');
		const mandatory = await filter(labelText?.children, 'input__mandatory');
		const message = await filter(label.children, 'input__message');

		for (const evt of events) {
			input.addEventListener(evt, (event) => {
				input.classList.remove('input--error');
				validate({ event, mandatory, message });
				if (event.target.value !== '') {
					labelText?.classList.add('input__label-text--top');
					return;
				}
				labelText?.classList.remove('input__label-text--top');
			}, false);
		}
	}
	
	async function validate({ event, mandatory, message }) {
		if (event.target.value.length > 0) {
			event.target.classList.remove('input--error');
			message.classList.remove('input__message--error');
			event.target.classList.add('input--success');
			if (event.target.value.length >= 1 && event.code === 'Backspace') {
				event.target.classList.remove('input--error');
				event.target.classList.remove('input--success');
				message.classList.remove('input__message--error');
			}
			return event.target.value.length;
		}

		if (mandatory?.classList.contains('input__mandatory')) {
			event.target.classList.remove('input--success');
			event.target.classList.add('input--error');
			message.classList.add('input__message--error');
			return event.target.value.length;
		}
		return 0;
	}

	function filter(childs, toFind) {
		for (const item of childs) {
			if (item.classList.contains(toFind)) {
				return item;
			}
		}
	}
}
