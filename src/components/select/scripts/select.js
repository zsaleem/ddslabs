export default function Select() {
	const options = document.querySelector('.input__options');
	const select = document.querySelector('.input__select');
	const opts = document.querySelectorAll('.input__options__opt');
	const events = 'focus blur'.split(' ');

	document.addEventListener('click', (event) => {
		if (!event.target.classList.contains('input__field')) {
			options.classList.remove('input__options--visible');
		}
	}, false);

	for (const evt of events) {
		select.addEventListener(evt, (event) => {
			if (evt === 'focus') {
				options.classList.add('input__options--visible');
			}
		}, false);
	}

	for (const opt of opts) {
		opt.addEventListener('click', (event) => {
			select.value = event.target.innerText;
			select.focus();
			options.classList.remove('input__options--visible');
			select.blur();
		}, false);
	}
}
