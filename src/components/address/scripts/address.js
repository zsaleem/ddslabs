export default async function Address() {
	const key = 'LkGVEyweK8i3Wp97Ajw5lcgVYNWOvPt71UeK6OEs39o';
	const address = document.querySelector('.address__field');
	const city = document.querySelector('.address__city');
	const zip = document.querySelector('.address__zip');
	const state = document.querySelector('.address__state');

	import('@here/maps-api-for-javascript')
		.then(({ default: H }) => {
			const platform = new H.service.Platform({
				'apikey': key,
			}); 
			const geocoder = platform.getSearchService();
			let geocodingParameters;

			address.addEventListener('input', () => {
				setLoading();
				geocodingParameters = {
					q: address.value, // '200 S Mathilda Sunnyvale CA',
				};
				
				if (address.value.length <= 1) {
					resetValues();
					setPlaceholder();
				} else {
					geocoder.geocode(
						geocodingParameters,
						(result) => {
							if (result.items.length === 1) {
								city.value = result.items[0].address.city || '';
								state.value = result.items[0].address.state || '';
								zip.value = result.items[0].address.postalCode || '';
							} else {
								setPlaceholder();
							}
						},
						(error) => {
							console.log(error);
						}
					);
				}
			}, false);
	})
	.catch(error => console.log(error));

	function setLoading() {
		city.value = 'loading city...';
		state.value = 'loading state...';
		zip.value = 'loading zip...';
	}

	function setPlaceholder() {
		city.placeholder = 'City *';
		state.placeholder = 'State *';
		zip.placeholder = 'Zip *';
	}

	function resetValues() {
		city.value = '';
		state.value = '';
		zip.value = '';
	}
}